import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType ;
import java.util.List;
import java.io.File;
import java.util.ArrayList;

// java -cp bin:img --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls Pendu
// javac -d bin --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls src/*.java
    
/**
 * Vue du jeu du pendu
 */
public class Pendu extends Application {
    /**
     * modèle du jeu
     **/
    private MotMystere modelePendu;
    /**
     * Liste qui contient les images du jeu
     */
    private ArrayList<Image> lesImages;
    /**
     * Liste qui contient les noms des niveaux
     */    
    public List<String> niveaux;

    // les différents contrôles qui seront mis à jour ou consultés pour l'affichage
    /**
     * le dessin du pendu
     */
    private ImageView dessin;
    /**
     * le mot à trouver avec les lettres déjà trouvé
     */
    private Text motCrypte;
    /**
     * la barre de progression qui indique le nombre de tentatives
     */
    private ProgressBar pg;
    /**
     * le clavier qui sera géré par une classe à implémenter
     */
    private Clavier clavier;
    /**
     * le text qui indique le niveau de difficulté
     */
    private Text leNiveau;
    /**
     * le chronomètre qui sera géré par une clasee à implémenter
     */
    private Chronometre chrono;
    /**
     * le panel Central qui pourra être modifié selon le mode (accueil ou jeu)
     */
    private BorderPane panelCentral;
    /**
     * le bouton Paramètre / Engrenage
     */
    private Button boutonParametres;
    /**
     * le bouton Accueil / Maison
     */    
    private Button boutonMaison;
    /**
     * le bouton qui permet de (lancer ou relancer une partie
     */ 
    private Button bJouer;
    private Button bouttonInformation;

    /**
     * initialise les attributs (créer le modèle, charge les images, crée le chrono ...)
     */
    @Override
    public void init() {
        //On charge un mot mystère
        this.modelePendu = new MotMystere("/usr/share/dict/american-english", 3, 10, MotMystere.FACILE, 10);
        //On charge les images
        this.lesImages = new ArrayList<Image>();
        this.chargerImages("./img");

        //On charge le dessin
        this.dessin = new ImageView(new Image("./pendu0.png"));

        //On charge le mot crypté 
        this.motCrypte = new Text(this.modelePendu.getMotCrypte());


        //On créer les bouttons
        ImageView home =  new ImageView(new Image("home.png",25,25,false,false));
        ImageView para =  new ImageView(new Image("parametres.png",25,25,false,false));
        ImageView info =  new ImageView(new Image("info.png",25,25,false,false));
        this.boutonMaison = new Button("",home);
        this.boutonParametres = new Button("",para);
        this.bouttonInformation = new Button("", info);
        this.bJouer = new Button("Lancer une partie");
        this.boutonMaison.setOnAction(new RetourAccueil(this.modelePendu,this));
        this.bouttonInformation.setOnAction(new ControleurInfos(this));
        this.bJouer.setOnAction(new ControleurLancerPartie(this.modelePendu,this));
        this.pg = new ProgressBar();
        this.pg.setProgress(0);
        // On créer le border pan
        this.panelCentral = new BorderPane();

        // On initialise le chronpomètre
        this.chrono = new Chronometre();

        
        //On créer le clavier
        String lettres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        this.clavier = new Clavier(lettres, new ControleurLettres(this.modelePendu, this));
        
        // On créer la progress bar
        
    }

    /**
     * @return  le graphe de scène de la vue à partir de methodes précédantes
     */
    private Scene laScene(){
        BorderPane fenetre = new BorderPane();
        fenetre.setTop(this.titre());
        fenetre.setCenter(this.panelCentral);
        return new Scene(fenetre, 800, 1000);
    }

    /**
     * @return le panel contenant le titre du jeu
     */
    public Pane titre(){
        // A implementer          
        Pane banniere = new Pane();
        return banniere;
    }

    /**
     * @return le chrono
     */
    public Chronometre getChronometre(){
        // A implementer          
        return this.chrono;
    }

    public Button getBoutonMaison() {
        return this.boutonMaison;
    }

    public Clavier getClavier() {
        return this.clavier;
    }

    public Text getMotCrypte() {
        return this.motCrypte;
    }

    public void setMotCrypte(Text motCrypte) {
        this.motCrypte = motCrypte;
    }


    

    /**
     * @return la fenêtre de jeu avec le mot crypté, l'image, la barre
     *         de progression et le clavier
     */
    private BorderPane fenetreJeu(){
        BorderPane res = new BorderPane();
        this.bJouer.setText("Nouveau mot");

        // ajouts de la partie haute

        ToolBar barreOutil = new ToolBar();
        BorderPane top = new BorderPane();
        Text text = new Text("Jeu du pendu ");
        this.boutonMaison.setDisable(false);
        text.setFont(Font.font("Arial", FontWeight.BOLD , 32) ) ;
        barreOutil.getItems().add(this.boutonMaison);
        barreOutil.getItems().add(this.bouttonInformation);
        barreOutil.getItems().add(this.boutonParametres);
        top.setRight(barreOutil);
        top.setLeft(text);
        top.setPadding(new Insets(10));
        top.setStyle("-fx-background-color:#e6e6fa;");
        res.setTop(top);

        // Ajout du centre

        VBox vboxCentre = new VBox();
        VBox.setMargin(this.pg, new Insets(10));
        vboxCentre.setAlignment(Pos.CENTER);
        vboxCentre.getChildren().addAll(this.motCrypte, this.dessin, this.pg,this.clavier);
        res.setCenter(vboxCentre);

        // Ajout de la partie Droite

        VBox vboxDroite = new VBox();
        this.leNiveau = (this.modelePendu.getNiveauString());
        // ajouter le chronomètre

        TitledPane chrono = new TitledPane();
        chrono.setText("Chronomètre");
        chrono.setAlignment(Pos.CENTER);
        chrono.setCollapsible(false);
        chrono.setContent(this.chrono);

        // ajout du boutton nouveau mot
        VBox.setMargin(this.bJouer, new Insets(10));
        VBox.setMargin(this.leNiveau, new Insets(10));
        vboxDroite.setAlignment(Pos.TOP_CENTER);
        vboxDroite.getChildren().addAll(this.leNiveau,chrono, this.bJouer);
        res.setRight(vboxDroite);

        return res;
    }

    /**
     * @return la fenêtre d'accueil sur laquelle on peut choisir les paramètres de jeu
     */
    private BorderPane fenetreAccueil(){
        this.bJouer.setText("lancer une partie");
        BorderPane res = new BorderPane();


        // ajouts de la partie haute

        ToolBar barreOutil = new ToolBar();
        BorderPane top = new BorderPane();
        Text text = new Text("Jeu du pendu ");
        this.dessin = new ImageView("pendu0.png");
        text.setFont(Font.font("Arial", FontWeight.BOLD , 32) ) ;
        this.boutonMaison.setDisable(true);
        barreOutil.getItems().add(this.boutonMaison);
        barreOutil.getItems().add(this.bouttonInformation);
        barreOutil.getItems().add(this.boutonParametres);
        top.setRight(barreOutil);
        top.setLeft(text);
        top.setPadding(new Insets(10));
        top.setStyle("-fx-background-color:#e6e6fa;");
        res.setTop(top);

        // Ajout du centre 

        VBox center = new VBox();
        VBox radioBouttons = new VBox();
        ToggleGroup groupeBouttons = new ToggleGroup();
        VBox.setMargin(radioBouttons, new Insets(10));


        RadioButton facile = new RadioButton("Facile");
        RadioButton medium = new RadioButton("Médium");
        RadioButton  difficile = new RadioButton("Difficile");
        RadioButton expert = new RadioButton("Expert");
        facile.setOnAction(new ControleurNiveau(this.modelePendu));
        medium.setOnAction(new ControleurNiveau(this.modelePendu));
        difficile.setOnAction(new ControleurNiveau(this.modelePendu));
        expert.setOnAction(new ControleurNiveau(this.modelePendu));
        facile.setSelected(true);  

        facile.setToggleGroup(groupeBouttons);
        medium.setToggleGroup(groupeBouttons);
        difficile.setToggleGroup(groupeBouttons);
        expert.setToggleGroup(groupeBouttons);

        radioBouttons.getChildren().addAll(facile,medium,difficile,expert);


        TitledPane menu = new TitledPane();
        menu.setText("Choisir la difficulté");
        menu.setContent(radioBouttons);

        center.getChildren().addAll(this.bJouer,menu);
        res.setCenter(center);

        return res;
    }

    /**
     * charge les images à afficher en fonction des erreurs
     * @param repertoire répertoire où se trouvent les images
     */
    private void chargerImages(String repertoire){
        for (int i=0; i<this.modelePendu.getNbErreursMax()+1; i++){
            File file = new File(repertoire+"/pendu"+i+".png");
            System.out.println(file.toURI().toString());
            this.lesImages.add(new Image(file.toURI().toString()));
        }
    }

    public void modeAccueil(){
        this.panelCentral.setCenter(this.fenetreAccueil());
    }
    
    public void modeJeu(){
        this.panelCentral.setCenter(this.fenetreJeu());
    }
    
    public void modeParametres(){
        // A implémenter
    }

    /** lance une partie */
    public void lancePartie(){
        this.modelePendu.setMotATrouver();
        this.getBoutonMaison().setDisable(true);
        this.getChronometre().resetTime();
        this.getChronometre().start();
        this.modeJeu();
        this.getClavier().activeTout();
        
    }

    /**
     * raffraichit l'affichage selon les données du modèle
     */
    public void majAffichage(){
        this.motCrypte.setText(this.modelePendu.getMotCrypte());
        this.dessin.setImage(this.lesImages.get(this.lesImages.size()-this.modelePendu.getNbErreursRestants()-1));
        this.clavier.desactiveTouches(this.modelePendu.getLettresEssayees());
        this.pg.setProgress(1-(float) this.modelePendu.getNbLettresRestantes()/this.modelePendu.getMotATrouve().length());
    }



    public Alert popUpPartieEnCours(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"La partie est en cours!\n Etes-vous sûr de l'interrompre ?", ButtonType.YES, ButtonType.NO);
        alert.setTitle("Attention");
        return alert;
    }
        
    public Alert popUpReglesDuJeu(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION," Les règles sont simples:\n \n "+
        "Tu as dix essais pour trouver le mot caché derrière les étoiles \n" +
        "Si tu trouves ça difficile tu peux revenir à l'acceuil et changer la difficulté ou bien changer de mots \n"+
        "A toi de trouver les lettres correct pour ne pas finir pendu !", ButtonType.OK);
        return alert;
    }
    
    public Alert popUpMessageGagne(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "Felicitation tu as gagné");        
        return alert;
    }
    
    public Alert popUpMessagePerdu(){
        // A implementer    
        Alert alert = new Alert(Alert.AlertType.INFORMATION,"Désolé... Tu as perdu, essaye encore", ButtonType.OK);
        return alert;
    }

    /**
     * créer le graphe de scène et lance le jeu
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("IUTEAM'S - La plateforme de jeux de l'IUTO");
        stage.setScene(this.laScene());
        this.modeAccueil();
        stage.show();
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }    
}
