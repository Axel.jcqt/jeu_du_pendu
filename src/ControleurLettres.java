import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

/**
 * Controleur du clavier
 */
public class ControleurLettres implements EventHandler<ActionEvent> {

    /**
     * modèle du jeu
     */
    private MotMystere modelePendu;
    /**
     * vue du jeu
     */
    private Pendu vuePendu;

    /**
     * @param modelePendu modèle du jeu
     * @param vuePendu vue du jeu
     */
    ControleurLettres(MotMystere modelePendu, Pendu vuePendu){
        this.vuePendu = vuePendu;
        this.modelePendu = modelePendu;
    }

    /**
     * Actions à effectuer lors du clic sur une touche du clavier
     * Il faut donc: Essayer la lettre, mettre à jour l'affichage et vérifier si la partie est finie
     * @param actionEvent l'événement
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        Button boutton = (Button) (actionEvent.getSource());
        char lettre = boutton.getText().charAt(0);
        this.modelePendu.essaiLettre(lettre);
        boutton.setDisable(true);
        this.vuePendu.majAffichage();
        if (this.modelePendu.gagne()){
            this.vuePendu.getChronometre().stop();
            this.vuePendu.getClavier().desactiveTout();
            this.vuePendu.popUpMessageGagne().showAndWait();
        }
        else if(this.modelePendu.perdu()){
            this.vuePendu.getChronometre().stop();
            this.vuePendu.getClavier().desactiveTout();
            this.vuePendu.popUpMessagePerdu().showAndWait();
        }
    }
}
