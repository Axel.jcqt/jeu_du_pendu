import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.TilePane;
import javafx.scene.shape.Circle ;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Génère la vue d'un clavier et associe le contrôleur aux touches
 * le choix ici est d'un faire un héritié d'un TilePane
 */
public class Clavier extends TilePane{

    private List<Button> clavier;


    /**
     * constructeur du clavier
     * @param lettres2 une chaine de caractères qui contient les lettres à mettre sur les touches
     * @param actionTouches le contrôleur des touches
     * @param tailleLigne nombre de touches par ligne
     */
    public Clavier(String lettres, EventHandler<ActionEvent> actionTouches) {
        // A implémenter
        super();
        this.setHgap(5);
        this.setVgap(5);
        this.setAlignment(Pos.CENTER_LEFT);
        this.setPadding(new Insets(20));
        this.clavier = new ArrayList<Button>();
        for(int i = 0 ; i < lettres.length(); i ++){
            Button touche = new Button(lettres.charAt(i) + "");
            touche.setOnAction(actionTouches);
            touche.setPrefWidth(50);
            touche.setShape(new Circle(1.5));
            this.getChildren().add(touche);
            this.clavier.add(touche);
        }
    }
        

    /**
     * permet de désactiver certaines touches du clavier (et active les autres)
     * @param touchesDesactivees une chaine de caractères contenant la liste des touches désactivées
     */
    public void desactiveTouches(Set<String> touchesDesactivees){
        for (Button bouton : this.clavier){
            if (touchesDesactivees.contains(bouton.getText())){
                bouton.cancelButtonProperty();
            }
        }
    }



    public void ActivesTouches(Set<String> touchesDesactivees){
        for (Button bouton : this.clavier){
            if (touchesDesactivees.contains(bouton.getText())){
                bouton.setDisable(false);
            }
        }
    }



    /**
     * permet de désactiver toutres les touches du clavier 
     */
    public void desactiveTout(){
        for (Button bouton : this.clavier){bouton.setDisable(true);}
    }


        /**
     * permet d'activer toutes les  touches du clavier 
     */
    public void activeTout(){
        for (Button bouton : this.clavier){bouton.setDisable(false);}
    }
}
