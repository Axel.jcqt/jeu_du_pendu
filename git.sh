#!/bin/bash
#script

git config --global credential.helper cache                  # Garde le mots de passe et identifiant en cache
git config --global credential.helper "cache --timeout=5800" # pour 1h30

echo " Séléctionner l'option qui vous convient"
echo " 1 - Push vos fichier sur Git"
echo " 2 - Création d'une branche sur le dépot local et distant"
echo " 3 - Faire le merge entre deux branches"
read choix

if [ $choix = 3 ]
then
    # Affichage des différentes branches à l'utilisateur
    echo " Voici les branches " && git branch
    # Choix des branches à Merge
    echo " Nom de la branche source" && read source1 && echo " Nom de la branche cible" && read cible
    # Mise à jour de l'origin et création de la branches dans le dépot local si elle n'existe pas
    git fetch origin && git checkout -b "$source1" "origin/$source1"
    git fetch origin
    git checkout "$cible"
    git merge --no-ff "$source1" || code .
    echo " Réglez les conflit en local sur Vscode"
    echo " Les conflit sont il réglés? y/n"
    read valider
    if [ $valider = 'y' ]
    then
        git add .
        echo "écrire votre message commit: "
        read mess
        git commit -m "$mess"
        git push origin "$cible"
        echo " Voulez vous supprimer la branche? y/n"
        read suppression
        if [ $valider = 'y' ]
        then
        #Suppression de la branche en local
        git branch -d $source1*
        #Suppression de la branche sur le dépot
        git push origin --delete $source1
        #Mise à jour du dépot distant
        git fetch --all -prune
        fi
    fi
else
    if [ $choix = 1 ]
    then
        git pull || git merge | code .
        git add .
        echo "écrire votre message commit: "
        read mess
        git commit -m "$mess"
        git push
    else
        if [ $choix = 2 ]
        then
            echo "Quel est le nom de la nouvelle branche: "
            read branch
            git switch -c $branch
            git push --set-upstream origin $branch
            git branch
        fi
    fi
fi
#Jacquet Axel